import csv
import copy

class HierarchyTree:
    def __init__(self, name, sort_units=True):
        self.name = name
        self._units = {'__root__': ['__root__','xxx', 0] }
        self._parents_set = set()
        self._unit_children_mapping = {}
        self._hierarchy = { 0: ['__root__', '__root__', 0] }
        self._temp_unit_children_mapping = {}
        self._sort_units = sort_units

    def get_first_child(self, unit_id):
        if unit_id in self._parents_set and len(self._temp_unit_children_mapping[unit_id]) > 0:
            return self._temp_unit_children_mapping[unit_id][0]
        return None

    def remove_child(self, unit_id, child_id):
        pos = self._temp_unit_children_mapping[unit_id].index(child_id)
        self._temp_unit_children_mapping[unit_id].pop(pos)

    def get_parent(self, unit_id):
        if unit_id == '__root__':
            return None
        return self._units[unit_id][1]

    def any_children_left(self, parent_id):
        if parent_id in self._parents_set and len(self._temp_unit_children_mapping[parent_id]) > 0:
            return True
        return False

    def get_nearest_parent_having_children_left(self, unit_id):
        while not self.any_children_left(unit_id):
            unit_id = self.get_parent(unit_id)
        return unit_id

    def get_children_list(self, unit_id):
        if unit_id in self._parents_set:
            return self._temp_unit_children_mapping[unit_id]

    def load_csv_data(self, csv_path, header=True):
        file = open(csv_path, newline='')
        reader = csv.reader(file)
        if header:
            next(reader)
        d = []
        for row in reader:
            self._units.update({ row[0]: [row[1], row[2], 0] })        

    def create_parents_set(self):
        for k in self._units:
            if self._units[k][1] not in self._parents_set:
                self._parents_set.add(self._units[k][1])

    def prepare_unit_children_structure(self):
        for k in self._parents_set:
            self._unit_children_mapping[k] = []
        self._unit_children_mapping['__root__'] = []

    def populate_unit_children_mapping(self):
        for k in self._units:
            self._unit_children_mapping[self._units[k][1]].append(k)
        if self._sort_units:
            pass
            # need to implement sorting by names here            
        self._temp_unit_children_mapping = copy.deepcopy(self._unit_children_mapping)

    def build_hierarchy(self):
        # initializing helper variables for traversing the tree
        a = 0
        parent = '__root__'
        curr_level = 0
        while True:
            if self.get_first_child(parent):
                a += 1
                child = self.get_first_child(parent)
                self._hierarchy.update({a : [child, self._units[child][0], curr_level + 1]})
                self._units[child][2] = curr_level + 1
                self.remove_child(parent,child)
            if self.any_children_left(child):
                parent = child
                curr_level = self._units[parent][2]
            else:
                try:
                    parent = self.get_nearest_parent_having_children_left(child)
                    curr_level = self._units[parent][2]
                except KeyError:
                    break

